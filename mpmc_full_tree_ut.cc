#include "mpmc_full_tree.hh"
#include "ut_helpers.hh"
#include "gtest/gtest.h"
#include <thread>
#include <algorithm>

using mt_queue::MPMC_FullTree;

TEST(FullTree, EnqueueDequeueSomeItems) {
    constexpr uintptr_t VAL_LIMIT = 1000000;
    MPMC_FullTree<void, false> queue;
    for (uintptr_t val = 42; val < VAL_LIMIT; ++val) {
	queue.Enqueue(reinterpret_cast<void*>(val));
    }
    for (uintptr_t val = 42; val < VAL_LIMIT; ++val) {
	auto elem = queue.Dequeue();
	ASSERT_NE(elem, nullptr);
	EXPECT_EQ(elem, reinterpret_cast<void*>(val));
    }
}


TEST(FullTree, Enqueue3Thread_Dequeue1Thread_PointerElem) {
    constexpr int NUMBER_OF_PRODUCERS = 3;
    constexpr int NUMBER_OF_ITEMS = 10000000;
    constexpr int DIFF_GAP = 10 * NUMBER_OF_ITEMS;

    MPMC_FullTree<void, false> myqueue;
    SyncStart sync(NUMBER_OF_PRODUCERS);

    auto producer_lambda = [&](int start_num) {
        sync.WaitForGreenLight();
        for (int i = 1; i <= NUMBER_OF_ITEMS; ++i)
            myqueue.Enqueue((void*)(uintptr_t)(i + start_num));
    };

    ::std::thread producers[NUMBER_OF_PRODUCERS];
    uintptr_t last_items[NUMBER_OF_PRODUCERS];
    for (int i = 0; i < NUMBER_OF_PRODUCERS; ++i) {
        producers[i] = ::std::thread(producer_lambda, i * DIFF_GAP);
        last_items[i] = i * DIFF_GAP;
    }

    sync.Start();
    for (size_t item_count = 0;
         item_count < NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;)
    {
        void* item = myqueue.Dequeue();
        if (item == nullptr)
            continue;

        uintptr_t item_int = reinterpret_cast<uintptr_t>(item);
        uintptr_t producer_num = item_int / DIFF_GAP;
        EXPECT_EQ(last_items[producer_num] + 1, item_int);
        last_items[producer_num] = item_int;
        ++item_count;
    }

    for (auto& producer: producers)
        producer.join();
}


TEST(FullTree, Enqueue2Threads_Dequeue2Threads) {
    constexpr uint16_t NUMBER_OF_PRODUCERS = 2;
    constexpr uint16_t NUMBER_OF_CONSUMERS = 2;
    constexpr uint32_t NUMBER_OF_ITEMS = 10000000;
    constexpr uint32_t TOTAL_NUMBER_OF_ITEMS =
	NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;
    constexpr uint64_t DIFF_GAP = 1UL << 32UL;

    MPMC_FullTree<void, false> myqueue;
    SyncStart sync(NUMBER_OF_PRODUCERS + NUMBER_OF_CONSUMERS);

    ::std::atomic<uint32_t> consume_counter = 0;
    ::std::atomic<uint32_t> source_counter = 1;

    auto producer_lambda = [&](uint64_t start_num) noexcept {
        sync.WaitForGreenLight();
        for (uint32_t i = 1; i <= NUMBER_OF_ITEMS; ++i) {
            auto num = source_counter.fetch_add(
                1UL, ::std::memory_order_seq_cst);
            myqueue.Enqueue((void*)(uintptr_t)(start_num + num));
        }
    };

    struct ConsumeRecord {
        uint16_t producer_num;
        uint16_t consumer_num;
        uint32_t serial;
        uint32_t consume_serial;
    };
    ::std::vector<ConsumeRecord> records[NUMBER_OF_CONSUMERS];
    for (auto& rec: records) {
      rec.reserve(TOTAL_NUMBER_OF_ITEMS);
    }

    auto consumer_lambda = [&](uint16_t consumer_num) noexcept {
        records[consumer_num].resize(TOTAL_NUMBER_OF_ITEMS);
        records[consumer_num].resize(0);
        ASSERT_EQ(records[consumer_num].capacity(), TOTAL_NUMBER_OF_ITEMS);
        sync.WaitForGreenLight();
        for (;;) {
            auto item_counter =
                consume_counter.load(::std::memory_order_acquire);
            if (item_counter == TOTAL_NUMBER_OF_ITEMS)
                break;
            auto elem = myqueue.Dequeue();
            if (elem == nullptr)
                continue;
            uint64_t ielem = reinterpret_cast<uint64_t>(elem);
            uint32_t consume_serial =
                consume_counter.fetch_add(1UL, ::std::memory_order_seq_cst);
            records[consumer_num].emplace_back(ConsumeRecord{
                    .producer_num = static_cast<uint16_t>(ielem >> 32UL),
                    .consumer_num = consumer_num,
                    .serial = static_cast<uint32_t>(ielem),
                    .consume_serial = consume_serial});
        }
    };

    ::std::thread producers[NUMBER_OF_PRODUCERS];
    for (uint16_t i = 0; i < NUMBER_OF_PRODUCERS; ++i)
        producers[i] = ::std::thread(producer_lambda, i * DIFF_GAP);

    ::std::thread consumers[NUMBER_OF_CONSUMERS];
    for (uint16_t i = 0; i < NUMBER_OF_CONSUMERS; ++i)
        consumers[i] = ::std::thread(consumer_lambda, i);

    sync.Start();

    for (auto& producer: producers)
        producer.join();
    for (auto& consumer: consumers)
        consumer.join();

    // verify consume records
    ::std::vector<ConsumeRecord> merged;
    merged.reserve(TOTAL_NUMBER_OF_ITEMS);
    for (auto& rec: records)
        merged.insert(merged.end(), rec.begin(), rec.end());
    auto compare_lambda =
        [](const ConsumeRecord& left, const ConsumeRecord& right) noexcept {
            return left.serial < right.serial;
        };
    ::std::sort(merged.begin(), merged.end(), compare_lambda);

    ::uint32_t expect = 1;
    for (auto& elem: merged) {
        ASSERT_EQ(elem.serial, expect);
        ++expect;
    }
}


TEST(FullTree, Enqueue2Threads_Dequeue2Threads_Perf) {
    constexpr uint16_t NUMBER_OF_PRODUCERS = 2;
    constexpr uint16_t NUMBER_OF_CONSUMERS = 2;
    constexpr uint32_t NUMBER_OF_ITEMS = 10000000;
    constexpr uint32_t TOTAL_NUMBER_OF_ITEMS =
	NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;
    constexpr uint64_t DIFF_GAP = 1UL << 32UL;

    MPMC_FullTree<void, false> myqueue;
    SyncStart sync(NUMBER_OF_PRODUCERS + NUMBER_OF_CONSUMERS);

    ::std::atomic<uint32_t> consume_counter = 0;

    auto producer_lambda = [&](uint64_t start_num) noexcept {
        sync.WaitForGreenLight();
        for (uint32_t i = 1; i <= NUMBER_OF_ITEMS; ++i) {
            myqueue.Enqueue((void*)(uintptr_t)(start_num + i));
        }
    };

    auto consumer_lambda = [&](uint16_t consumer_num) noexcept {
        sync.WaitForGreenLight();
        for (;;) {
          auto elem = myqueue.Dequeue();
          if (elem == nullptr) {
            auto item_counter =
              consume_counter.load(::std::memory_order_acquire);
            if (item_counter == TOTAL_NUMBER_OF_ITEMS)
              break;
            continue;
          }
          uint32_t consume_serial =
            consume_counter.fetch_add(1UL, ::std::memory_order_seq_cst);
          if (consume_serial == TOTAL_NUMBER_OF_ITEMS - 1)
            break;
        }
    };

    ::std::thread producers[NUMBER_OF_PRODUCERS];
    for (uint16_t i = 0; i < NUMBER_OF_PRODUCERS; ++i)
        producers[i] = ::std::thread(producer_lambda, i * DIFF_GAP);

    ::std::thread consumers[NUMBER_OF_CONSUMERS];
    for (uint16_t i = 0; i < NUMBER_OF_CONSUMERS; ++i)
        consumers[i] = ::std::thread(consumer_lambda, i);

    sync.Start();

    for (auto& producer: producers)
        producer.join();
    for (auto& consumer: consumers)
        consumer.join();
}
