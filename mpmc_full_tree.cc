/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "mpmc_full_tree.hh"
#include <cstdlib>
#include <cstring>
#include <memory>

namespace mt_queue {


struct MPMC_FullTree_Raw::LeafLink final {
    ::std::atomic<TreeNode*> link;
    ::std::atomic<::uintptr_t> visiter_counter;
};


struct MPMC_FullTree_Raw::TreeNode final {
    ::std::atomic<::uint64_t> consumed{0};
    ::std::atomic<void*> items[0];
    LeafLink leaves[0];

    static TreeNode*
    AllocateLeaf(::size_t number_of_elements) noexcept {
        const ::size_t elements_storage =
            sizeof(items[0]) * number_of_elements;
        void* mem = ::std::malloc(
            sizeof(TreeNode) + elements_storage);
        if (mem == nullptr)
            ::std::abort();
        TreeNode* result = new (mem) TreeNode;
        ::std::memset(&result->items, 0, elements_storage);
        return result;
    }

    static TreeNode*
    AllocateMediate(::size_t number_of_elements) noexcept {
        const ::size_t elements_storage =
            sizeof(leaves[0]) * number_of_elements;
        void* mem = ::std::malloc(
            sizeof(TreeNode) + elements_storage);
        if (mem == nullptr)
            ::std::abort();
        TreeNode* result = new (mem) TreeNode;
        ::std::memset(&result->leaves, 0, elements_storage);
        return result;
    }
};


struct MPMC_FullTree_Raw::DequeueWalkContext {
    LeafLink* slots[MAX_BITS];
    TreeNode* nodes[MAX_BITS];
    ::uint8_t count = 0;
};


MPMC_FullTree_Raw::MPMC_FullTree_Raw(const ::uint8_t bits_per_level) noexcept
    : bits_per_level_{bits_per_level}
    , root_{TreeNode::AllocateMediate(node_item_count_)}
{
    if (bits_per_level_ > MAX_BITS)
        ::std::abort();
    if (MAX_BITS % bits_per_level_ != 0)
        // is not supported yet
        ::std::abort();
}


MPMC_FullTree_Raw::~MPMC_FullTree_Raw() {
    ReclaimAll();
}


void MPMC_FullTree_Raw::ReclaimAll() noexcept {
    TreeNode* node_stack[MAX_BITS];
    ::uint64_t scan_pos_stack[MAX_BITS];
    ::uint8_t stack_count = 0;

    TreeNode* walk = root_;
    ::uint64_t scan_pos = 0;
    ::uint8_t level = bits_per_level_ * 2;
    for (;;) {
        for (; scan_pos < node_item_count_;) {
            TreeNode* child = walk->leaves[scan_pos].link.load(
                ::std::memory_order_relaxed);
            if (child == nullptr) {
                ++scan_pos;
                continue;
            }
            if (level == MAX_BITS) {
                ::std::free(child);
                ++scan_pos;
                continue;
            }
            node_stack[stack_count] = walk;
            scan_pos_stack[stack_count] = scan_pos + 1;
            ++stack_count;
            walk = child;
            scan_pos = 0;
            level += bits_per_level_;
        }
        ::std::free(walk);
        if (stack_count == 0)
            break;
        --stack_count;
        walk = node_stack[stack_count];
        scan_pos = scan_pos_stack[stack_count];
        level -= bits_per_level_;
    }
}


void MPMC_FullTree_Raw::Enqueue(void* payload) noexcept {
    if (payload == nullptr)
        ::std::abort();
    // get a ticket
    ::uint64_t ticket = ticket_source_.fetch_add(
        1U, ::std::memory_order_acquire);

    // find a ticket slot
    TreeNode* walk = root_;
    ::uint64_t bits = ticket;
    TreeNode* cache = nullptr;
    for (::uint8_t level = bits_per_level_ * 2; level < MAX_BITS;) {
        ::uint64_t choose_bits = bits >> (MAX_BITS - bits_per_level_);
        ::std::atomic<TreeNode*>* target = &walk->leaves[choose_bits].link;
        TreeNode* next_node = target->load(::std::memory_order_acquire);
        if (next_node == nullptr) {
            next_node = TreeNode::AllocateMediate(node_item_count_);
            TreeNode* expect = nullptr;
            if (!target->compare_exchange_strong(expect, next_node)) {
                if (cache != nullptr) {
                    ::std::free(cache);
                }
                cache = next_node;
                next_node = expect;
            }
        }
        walk = next_node;
        bits <<= bits_per_level_;
        level += bits_per_level_;
    }
    if (cache != nullptr) {
        ::std::free(cache);
    }

    ::uint64_t choose_bits = bits >> (MAX_BITS - bits_per_level_);
    ::std::atomic<TreeNode*>* leaf_link = &walk->leaves[choose_bits].link;
    TreeNode* leaf = leaf_link->load(::std::memory_order_acquire);
    if (leaf == nullptr) {
        leaf = TreeNode::AllocateLeaf(node_item_count_);
        TreeNode* expect = nullptr;
        if (!leaf_link->compare_exchange_strong(expect, leaf)) {
            ::std::free(leaf);
            leaf = expect;
        }
    }

    bits <<= bits_per_level_;
    bits >>= (MAX_BITS - bits_per_level_);
    leaf->items[bits].store(payload, std::memory_order_release);

    enqueue_ready_count_.fetch_add(1UL, ::std::memory_order_release);
}


MPMC_FullTree_Raw::TreeNode* MPMC_FullTree_Raw::Enter(
    DequeueWalkContext& walk_ctx, ::uint64_t target_slot) noexcept
{
    TreeNode* walk = root_;
    ::uint64_t bits = target_slot;
    ::uint8_t follow_stack_count = 0;
    ::uint8_t level = bits_per_level_;
    while (level < MAX_BITS) {
        if (follow_stack_count == walk_ctx.count)
            break;
        ::uint64_t choose_bits = bits >> (MAX_BITS - bits_per_level_);
        LeafLink* next_slot = &walk->leaves[choose_bits];
        if (next_slot != walk_ctx.slots[follow_stack_count]) {
            Leave(walk_ctx, false, follow_stack_count);
            if (walk_ctx.count < follow_stack_count)
                return nullptr;
            break;
        }
        walk = next_slot->link.load(::std::memory_order_relaxed);
        ++follow_stack_count;
        bits <<= bits_per_level_;
        level += bits_per_level_;
    }

    while (level < MAX_BITS) {
        ::uint64_t choose_bits = bits >> (MAX_BITS - bits_per_level_);
        TreeNode* next_walk = walk->leaves[choose_bits].link.load(
            ::std::memory_order_acquire);
        if (next_walk == nullptr)
            return nullptr;
        ::uint64_t counter =
              walk->leaves[choose_bits].visiter_counter.fetch_add(
                  4UL, ::std::memory_order_relaxed);
        if (counter & 1UL)
            // the node is reclaimed
            return nullptr;
        walk_ctx.slots[walk_ctx.count] = &walk->leaves[choose_bits];
        walk_ctx.nodes[walk_ctx.count] = walk;
        ++walk_ctx.count;
        if (counter & 2UL)
            // the node is marked for reclamation
            return nullptr;
        bits <<= bits_per_level_;
        level += bits_per_level_;
        walk = next_walk;
    }
    return walk;
}


bool MPMC_FullTree_Raw::ReclaimNode(LeafLink* slot) noexcept {
    ::uint64_t expect_no_visitors = 2UL;
    // reclaim the node if no other visitors
    bool success = slot->visiter_counter.compare_exchange_strong(
	expect_no_visitors, 1UL,
	::std::memory_order_acquire,
	::std::memory_order_relaxed);
    if (!success)
	return false;
    TreeNode* reclaim_ptr =
	slot->link.exchange(nullptr, ::std::memory_order_relaxed);
    ::std::free(reclaim_ptr);
    return true;
}


void MPMC_FullTree_Raw::Leave(
    DequeueWalkContext& walk_ctx,
    bool reclaim_top,
    ::uint8_t stack_target) noexcept
{
    while (walk_ctx.count > stack_target || reclaim_top) {
	--walk_ctx.count;
	bool node_reclaimed = false;
	if (reclaim_top) {
            // only one thread marks for reclamation
            // the thread consumes last item or child node
	    reclaim_top = false;
	    ::uint64_t prev = walk_ctx.slots[walk_ctx.count]->
		visiter_counter.fetch_sub(2UL, ::std::memory_order_seq_cst);
	    if (prev == 4UL)
		node_reclaimed = ReclaimNode(walk_ctx.slots[walk_ctx.count]);
	} else {
            // the node that this thread leaves is marked for reclamation
            // and this thread is the last visitor to the node
            // therefore this thread must reclaim the node
	    ::uint64_t prev = walk_ctx.slots[walk_ctx.count]->
		visiter_counter.fetch_sub(4UL, ::std::memory_order_release);
	    if (prev == 6UL)
		node_reclaimed = ReclaimNode(walk_ctx.slots[walk_ctx.count]);
	}
	if (node_reclaimed) {
	    ::uint64_t counter =
		walk_ctx.nodes[walk_ctx.count]->consumed.fetch_add(1UL) + 1UL;
	    if (counter == (1UL << bits_per_level_))
		reclaim_top = true;
	}
    }
}


[[nodiscard]] void* MPMC_FullTree_Raw::Dequeue() noexcept {
    ::uint64_t limit = enter_limit_.fetch_add(1UL, ::std::memory_order_relaxed);
    if (limit >= enqueue_ready_count_.load(::std::memory_order_acquire)) {
	enter_limit_.fetch_sub(1UL, ::std::memory_order_relaxed);
	return nullptr;
    }
    ::uint64_t front = dequeue_front_.load(::std::memory_order_acquire);
    ::uint64_t run_limit = ticket_source_.load(::std::memory_order_acquire);
    DequeueWalkContext walk_ctx;
    void* bung = reinterpret_cast<void*>(-1UL);

    for (;;) {
        void* payload;
        TreeNode* walk;
        walk = Enter(walk_ctx, front);
        if (walk) {
            ::uint64_t choose_bits = front & ((1UL << bits_per_level_) - 1);
	    payload = walk->items[choose_bits].load(
		::std::memory_order_acquire);
	    if (payload != bung && payload != nullptr) {
		bool done = walk->items[choose_bits].compare_exchange_strong(
		    payload, bung,
		    ::std::memory_order_acquire,
		    ::std::memory_order_acquire);
		if (done) {
		    ::uint64_t consumed = walk->consumed.fetch_add(
			1UL, ::std::memory_order_relaxed) + 1;
		    Leave(walk_ctx, consumed == (1UL << bits_per_level_));
		    return payload;
		}
		// here payload == bung
	    }
	} else {
	    payload = bung;
	}

	::uint64_t recent_front =
              dequeue_front_.load(::std::memory_order_acquire);
	if (recent_front > front) {
	    front = recent_front;
	    run_limit = ticket_source_.load(::std::memory_order_acquire);
	}
	if (payload == bung && recent_front == front) {
	    ++front;
	    dequeue_front_.compare_exchange_strong(
		recent_front, front,
		::std::memory_order_relaxed,
		::std::memory_order_relaxed);
	} else {
	    ++front;
	}
	if (front == run_limit) {
	    front = recent_front;
	    run_limit = ticket_source_.load(::std::memory_order_acquire);
	}
    }
}


} // namespace mt_queue
