/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once
#include <atomic>
#include "base_common.hh"

namespace mt_queue {


static constexpr ::uint16_t CACHE_LINE_SIZE = 64;


class MPMC_FullTree_Raw: public NoCopyable {
public:
    explicit MPMC_FullTree_Raw(const ::uint8_t bits_per_level = 16) noexcept;
    ~MPMC_FullTree_Raw();
    void Enqueue(void* payload) noexcept;
    [[nodiscard]] void* Dequeue() noexcept;

    MPMC_FullTree_Raw(MPMC_FullTree_Raw&) = delete;
    auto operator=(MPMC_FullTree_Raw) = delete;

protected:
    static constexpr uint8_t MAX_BITS = 64;

    struct LeafLink;
    struct TreeNode;
    struct DequeueWalkContext;

    TreeNode* Enter(
        DequeueWalkContext& walk_ctx, ::uint64_t target_slot) noexcept;
    void Leave(
        DequeueWalkContext& walk_ctx,
        bool reclaim_top = false,
        ::uint8_t stack_target = 0) noexcept;
    bool ReclaimNode(LeafLink* slot) noexcept;

    void ReclaimAll() noexcept;

    const ::uint8_t bits_per_level_;
    const ::uint64_t node_item_count_ = 1UL << bits_per_level_;
    TreeNode* const root_;
    char padding1[CACHE_LINE_SIZE - sizeof(::uint64_t)];
    ::std::atomic<::uint64_t> ticket_source_{0UL};
    char padding2[CACHE_LINE_SIZE - sizeof(::uint64_t)];
    ::std::atomic<::uint64_t> enqueue_ready_count_{0UL};
    char padding3[CACHE_LINE_SIZE - sizeof(::uint64_t)];
    ::std::atomic<::uint64_t> dequeue_front_{0UL};
    char padding4[CACHE_LINE_SIZE - sizeof(::uint64_t)];
    ::std::atomic<::uint64_t> enter_limit_{0UL};
    char padding5[CACHE_LINE_SIZE - sizeof(::uint64_t)];
};


template <class PayloadType, bool DeletePointerPayload = true>
class MPMC_FullTree: protected MPMC_FullTree_Raw {
public:
    explicit MPMC_FullTree(const ::uint8_t bits_per_level = 16) noexcept
        : MPMC_FullTree_Raw{bits_per_level}
    {}

    ~MPMC_FullTree() {
        if constexpr (DeletePointerPayload) {
            while (PayloadType* i = Dequeue()) {
                delete i;
	    }
	}
    }

    void Enqueue(PayloadType* payload) noexcept {
	MPMC_FullTree_Raw::Enqueue(payload);
    }

    [[nodiscard]] PayloadType* Dequeue() noexcept {
	return reinterpret_cast<PayloadType*>(MPMC_FullTree_Raw::Dequeue());
    }
};


} // namespace mt_queue
