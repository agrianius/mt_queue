#pragma once
#include <array>
#include <atomic>
#include <cstdint>
#include <memory>
#include <new>
#include <vector>
#include <utility>

namespace mt_queue {


template <class Type>
struct CachlinePadding {
  static constexpr ::uint16_t CACHE_LINE_SIZE = 64;
  char padding[CACHE_LINE_SIZE - sizeof(Type)];
};


template <class ITEM_TYPE>
struct DefaultTypeSpec {
  static constexpr ITEM_TYPE NoValue() noexcept {
    return ITEM_TYPE{0};
  }
};




template <class ITEM_TYPE, ::uint32_t CHUNK_SIZE>
struct Chunk {
  using ThreadCounterType = ::uint32_t;
  using SlotCounterType = ::uint16_t;

  ::std::atomic<::uint64_t> prev_chunk_id;
  ::std::atomic<::uint64_t> next_chunk_id;
  ::uint64_t first_ticket;
  CachlinePadding<ThreadCounterType> pad1;
  ::std::atomic<ThreadCounterType> use_counter{0};
  CachlinePadding<ThreadCounterType> pad2;
  ::std::atomic<SlotCounterType> consume_counter;
  CachlinePadding<ThreadCounterType> pad3;

  ::std::array<::std::atomic<ITEM_TYPE>, CHUNK_SIZE> elems;
  ::std::array<::std::atomic<::uint64_t>, CHUNK_SIZE> read_lock;
};


template <class ITEM_TYPE, ::size_t CHUNK_SIZE = 2048, class TypeSpec = DefaultTypeSpec<ITEM_TYPE>>
class BufferChainWithTickets {
public:
  using ChunkType = Chunk<ITEM_TYPE, CHUNK_SIZE>;
  using ThreadCounterType = ChunkType::ThreadCounterType;
  using SlotCounterType = ChunkType::SlotCounterType;

  BufferChain(::size_t capacity, ThreadCounterType number_of_threads) noexcept {
    number_of_chunks_ = (capacity - 1) / CHUNK_SIZE + 1 + (number_of_threads - 1) * 2;
    chunks_.reset(new (std::nothrow) ChunkType[number_of_chunks_]);
    // lock first chunk
    auto* first_chunk = &(chunks_.get()[0]);
    ResetChunk(first_chunk, 0, NULL_CHUNK);
    // minus one USE_BIT from previous chunk
    // one USE_BIT from enqueue_chunk_
    // one USE_BIT from dequeue_chunk_
    first_chunk->use_counter.fetch_add(USE_BIT * 2, ::std::memory_order_relaxed);
  }

  void Enqueue(ITEM_TYPE val) noexcept {
    const ::uint64_t my_ticket =
      enqueue_ticket_.fetch_add(1UL, ::std::memory_order_relaxed);
    for (;;) {
      ::uint64_t my_chunk_id = enqueue_chunk_.load(::std::memory_order_acquire);
      auto* my_chunk = &chunks_.get()[my_chunk_id];
      // use-lock my chunk
      {
        auto in_use =
          my_chunk->use_counter.fetch_add(USE_BIT, ::std::memory_order_acquire);
        if ((in_use & LOCK_BIT) == 0) {
          // this chunk has been reclaimed
          ReleaseChunk(my_chunk, USE_BIT);
          continue;
        }
      }
      if (my_ticket < my_chunk.first_ticket) {
        do {
          my_chunk_id = JumpBackward(my_chunk_id);
          my_chunk = &chunks_.get()[my_chunk_id];
        } while (my_ticket < my_chunk.first_ticket);
      } else if (my_ticket >= my_chunk.first_ticket + CHUNK_SIZE) {
        do {
          auto next_chunk_id = JumpForward(my_chunk_id);
          if (next_chunk_id == NULL_CHUNK) {
            next_chunk_id = FindNextFreeChunk(my_chunk_id);
          }
          bool success = enqueue_chunk_.compare_exchange_strong(
              my_chunk_id, next_chunk_id,
              ::std::memory_order_relaxed, ::std::memory_order_relaxed);
          if (success) {
            ReleaseChunk(my_chunk, USE_BIT);
          }
          my_chunk_id = next_chunk_id;
          my_chunk = &chunks_.get()[my_chunk_id];
          if (success) {
            my_chunk->use_counter.fetch_add(USE_BIT, ::std::memory_order_relaxed);
          }
        } while (my_ticket >= my_chunk.first_ticket + CHUNK_SIZE);
      }
      auto* my_slot = &my_chunk->elems[my_ticket - my_chunk->first_ticket];
      my_slot->store(val, ::std::memory_order_release);
      // explicitly: do not decrement USE_BIT for my_chunk
      number_of_ready_elements_.fetch_add(1L, ::std::memory_order_acq_rel);
      return;
    }
  }

  ITEM_TYPE Dequeue() noexcept {
    {
      // first we have to find out if there were enough elements
      // in a queue to read
      ::int32_t ready =
        number_of_ready_elements_.fetch_sub(1L, ::std::memory_order_acq_rel);
      if (ready <= 0) {
        number_of_ready_elements_.fetch_add(1L, ::std::memory_acquire_release);
        return NO_VALUE;
      }
    }

    // get a ticket and find a chunk with my slot
    ::uint64_t base_ticket =
      dequeue_ticket_.fetch_add(1UL, ::std::memory_order_relaxed);

    // get any chunk in a chunk chain
  jump_into_chunk_chain_again:
    ::uint64_t base_chunk_id = dequeue_chunk_.load(::std::memory_order_acquire);
    ChunkType* base_chunk = &chunks_.get()[base_chunk_id];
    {
      auto in_use = base_chunk->use_counter.fetch_add(
          USE_BIT, ::std::memory_order_acquire);
      if ((in_use & LOCK_BIT) == 0) {
        // this chunk has been reclaimed, try again
        ReleaseChunk(base_chunk, USE_BIT);
        goto get_into_chunk_chain_again;
      }
    }

    // walk throug chunk chain and find my base chunk
    if (base_ticket < base_chunk.first_ticket) {
      do {
        base_chunk_id = JumpBackward(base_chunk_id);
        base_chunk = &chunks_.get()[base_chunk_id];
      } while (base_ticket < base_chunk->first_ticket);
    } else if (base_ticket >= base_chunk->first_ticket + CHUNK_SIZE) {
      do {
        auto next_chunk_id = JumpForward(base_chunk_id);
        bool success = dequeue_chunk_.compare_exchange_strong(
            base_chunk_id, next_chunk_id,
            ::std::memory_order_relaxed, ::std::memory_order_relaxed);
        if (success) {
          ReleaseChunk(base_chunk, USE_BIT);
        }
        base_chunk_id = next_chunk_id;
        base_chunk = &chunks_.get()[base_chunk_id];
        if (success) {
          base_chunk->use_counter.fetch_add(USE_BIT, ::std::memory_order_relaxed);
        }
      } while (my_ticket >= my_chunk->first_ticket + CHUNK_SIZE);
    }

  begin_from_the_base_again:
    ::uint64_t seek_ticket = base_ticket;
    ::uint64_t seek_chunk_id = base_chunk_id;
    ChunkType* seek_chunk = base_chunk;

  seek_slot_again:
    auto& slot = seek_chunk.elems[seek_ticket - seek_chunk->first_ticket];
    ITEM_TYPE item = slot.load(::std::memory_order_acquire);
    if (item == NO_VALUE) {
    seek_next_slot:
      // this slot is not ready yet, seek next slot
      if (++seek_ticket >= seek_chunk->first_ticket + CHUNK_SIZE) {
        seek_chunk_id = JumpForward(seek_chunk_id);
        seek_chunk = &chunks_.get()[seek_chunk_id];
        if (seek_ticket < seek_chunk->first_ticket) {
          seek_ticket = seek_chunk->first_ticket;
        }
      }
      goto seek_slot_again;
    }

    // this slot is ready, let's lock it
    ::uint64_t expect_no_lock = NO_READ_LOCK;
    bool success = my_chunk->read_lock.compare_exchange_strong(
        expect_no_lock, base_ticket,
        ::std::memory_order_relaxed, ::std::memory_order_relaxed);
    if (success) {
      // this slot was successfully locked, almost done
      if (seek_ticket == base_ticket) {
        // this slot is my base slot, let's consume it
        ConsumeSlotInChunk(base_chunk);
        my_chunk->use_counter.fetch_sub(2 * USE_BIT, ::std::memory_order_acq_rel);
      } else {
        my_chunk->use_counter.fetch_sub(USE_BIT, ::std::memory_order_acq_rel);
      }
      return item;
    }
    // this slot has been locked by another consumer
    if (seek_ticket == base_ticket) {
      // this was my base slot, we should rebase
      ConsumeSlotInChunk(base_chunk);
      base_chunk->use_counter.fetch_sub(USE_BIT, ::std::memory_order_acq_rel);
      base_ticket = expect_no_lock;  // compare_exchage read current value
      // find base chunk, it must be one of previous chunks
      while (base_ticket < base_chunk->first_ticket) {
        base_chunk_id = JumpBackward(base_chunk_id);
        base_chunk = &chunks_.get()[base_chunk_id];
      }
      goto begin_from_the_base_again;
    }
    goto seek_next_slot;
  }

  static constexpr ITEM_TYPE NO_VALUE = TypeSpec::NoValue();

private:
  static constexpr ThreadCounterType USE_BIT = 4;
  static constexpr ThreadCounterType DETACH_BIT = 2;
  static constexpr ThreadCounterType LOCK_BIT = 1;
  static constexpr ::uint64_t NULL_CHUNK = -1UL;
  static constexpr ::uint64_t NO_READ_LOCK = -1UL;

  ::uint64_t number_of_chunks_;
  ::std::unique_ptr<ChunkType[]> chunks_;

  CachlinePadding<::uint64_t> pad0;
  ::std::atomic<::uint64_t> dequeue_chunk_{0};
  CachlinePadding<::uint64_t> pad1;
  ::std::atomic<::uint64_t> dequeue_ticket_{0};
  CachlinePadding<::uint64_t> pad2;
  ::std::atomic<::uint64_t> enqueue_chunk_{0};
  CachlinePadding<::uint64_t> pad3;
  ::std::atomic<::uint64_t> enqueue_ticket_{0};
  CachlinePadding<::uint64_t> pad4;
  ::std::atomic<::int64_t> number_of_ready_elements_{0};
  CachlinePadding<::int64_t> pad5;
  ::std::atomic<::uint64_t> detach_counter_{0};

  void ResetChunk(ChunkType* chunk, ::uint64_t first_ticket, ::uint64_t prev_chunk_id) noexcept {
    ::std::fill(::std::begin(chunk->elems), ::std::end(chunk->elems), NO_VALUE);
    ::std::fill(::std::begin(chunk->readlock), ::std::end(chunk->readlock), NO_READ_LOCK);
    chunk->use_counter.store(LOCK_BIT, ::std::memory_order_relaxed);
    chunk->first_ticket = first_ticket;
    chunk->prev_chunk_id.store(prev_chunk, ::std::memory_order_relaxed);
    chunk->next_chunk_id.store(NULL_CHUNK, ::std::memory_order_relaxed);
    // one USE_BIT from previous chunk
    // one USE_BIT from next chunk
    chunk->consume_counter.fetch_add(USE_BIT * 2, ::std::memory_order_relaxed);
  }

  ::uint64_t JumpBackward(::uint64_t my_chunk_id) noexcept {
    auto* my_chunk = chunks_.get()[my_chunk_id];
    for (;;) {
      auto jump_chunk_id = my_chunk->prev_chunk_id.load(::std::memory_order_acquire);
      auto* jump_chunk = chunks_.get()[jump_chunk_id];
      auto in_use = jump_chunk->fetch_add(USE_BIT, ::std::memory_order_acquire);
      if ((in_use & LOCK_BIT) == 0) {
        jump_chunk->fetch_sub(USE_BIT, ::std::memory_order_acq_rel);
        continue;
      }
      my_chunk->use_counter.fetch_sub(USE_BIT, ::std::memory_order_acq_rel);
      return jump_chunk_id;
    }
  }

  ::uint64_t JumpForward(::uint64_t my_chunk_id) noexcept {
    auto* my_chunk = chunks_.get()[my_chunk_id];
    for (;;) {
      auto jump_chunk_id = my_chunk->next_chunk_id.load(::std::memory_order_acquire);
      if (jump_chunk_id == NULL_CHUNK) {
        return NULL_CHUNK;
      }
      auto* jump_chunk = chunks_.get()[jump_chunk_id];
      auto in_use = jump_chunk->fetch_add(USE_BIT, ::std::memory_order_acquire);
      if ((in_use & LOCK_BIT) == 0) {
        jump_chunk->fetch_sub(USE_BIT, ::std::memory_order_acq_rel);
        continue;
      }
      my_chunk->use_counter.fetch_sub(USE_BIT, ::std::memory_order_acq_rel);
      return jump_chunk_id;
    }
  }

  ::uint64_t FindNextFreeChunk(::uint64_t const tail_chunk_id) noexcept {
    auto* const tail_chunk = &chunks_.get()[tail_chunk_id];
    ::uint64_t next_chunk_id =
        tail_chunk->next_chunk.load(::std::memory_order_acquire);
    if (next_chunk_id != NULL_CHUNK) {
      ::uint64_t expect = tail_chunk_id;
      if (enqueue_chunk_.compare_exchange_strong(
          expect, next_chunk_id,
          ::std::memory_order_relaxed, ::std::memory_order_acquire)) {
        return next_chunk_id;
      } else {
        return expect;
      }
    }
    // we have to find new chunk
    next_chunk_id = tail_chunk_id;
    for (;;) {
      ++next_chunk_id;
      next_chunk_id %= number_of_chunks_;
      auto* next_chunk = &chunks_.get()[next_chunk_id];
      if (next_chunk->use_counter.load(::std::memory_order_acquire) != 0) {
        continue;
      }
      ThreadCounterType expect = 0;
      bool success = next_chunk->use_counter.compare_exchange_strong(
          expect, LOCK_BIT,
          ::std::memory_order_acq_rel, ::std::memory_order_relaxed);
      if (!success) {
        continue;
      }
      ResetChunk(next_chunk, tail_chunk->first_ticket + CHUNK_SIZE, tail_chunk_id);
      ::uint64_t expect_null_chunk = NULL_CHUNK;
      success = tail_chunk->next_chunk_id.compare_exchange_strong(
          expect_null_chunk, next_chunk_id,
          ::std::memory_order_acq_rel, ::std::memory_order_acquire);
      if (success) {
        // make a fake consumption for tail chunk
        ConsumeSlotInChunk(tail_chunk);
        ::uint64_t expect_tail_chunk_id = tail_chunk_id;
        if (enqueue_chunk_.compare_exchange_strong(
                expect_tail_chunk_id, next_chunk_id,
                ::std::memory_order_acq_rel, ::std::memory_order_acquire)) {
          return next_chunk_id;
        } else {
          return expect_tail_chunk_id;
        }
      } else {
        next_chunk->use_counter.fetch_sub(LOCK_BIT, ::std::memory_order_acq_rel);
        ::uint64_t expect_enqueue_chunk_id = tail_chunk_id;
        if (enqueue_chunk_.compare_exchange_strong(
                expect_enqueue_chunk_id, expect_null_chunk,
                ::std::memory_order_relaxed, ::std::memory_order_acquire)) {
          return expect_null_chunk;
        } else {
          return expect_enqueue_chunk_id;
        }
      }
    }
  }

  void ReleaseChunk(ChunkType* chunk, ChunkType::ThreadCounterType cnt) noexcept {
    auto in_use = chunk->use_counter.fetch_sub(cnt, ::std::memory_order_release);
    if (in_use != LOCK_BIT | DETACH_BIT | cnt)
      return;
    in_use = LOCK_BIT | DETACH_BIT;
    chunk->use_counter.compare_exchange_strong(
        in_use, 0, ::std::memory_order_release, ::std::memory_order_relaxed);

  }

  void ConsumeSlotInChunk(ChunkType* chunk) noexcept {
    auto consumed = my_chunk->consume_counter.fetch_add(
        1U, ::std::memory_order_release) + 1;
    if (consumed == CHUNK_SIZE + 1) {
      // all slots in this chunk are consumed
      // set detach bit on this chunk
      chunk->use_counter.fetch_add(DETACH_BIT, ::std::memory_order_relaxed);
      detach_counter_.fetch_add(1U, ::std::memory_order_release);
    }
  }
};


}  // namespace mt_queue
