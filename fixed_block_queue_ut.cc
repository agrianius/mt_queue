/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "gtest/gtest.h"
#include "fixed_block_queue.hh"
#include <new>
#include <set>
#include <exception>


using mt_queue::I_FixedSizeAllocator;
using mt_queue::FixedBlockQueue;


namespace {
    template <size_t SIZE>
    class AllocatorMock: public I_FixedSizeAllocator {
    public:
        virtual void* alloc() override {
            auto result = ::operator new(SIZE);
            //printf("allocated: %p\n", result);
            return result;
        }

        virtual void free(void* ptr) override {
            //printf("deleted: %p\n", ptr);
            return ::operator delete(ptr);
        }

        virtual size_t getBlockSize() override {
            return SIZE;
        }
    };
}


TEST(FixedBlockQueue, EnqueueDequeue) {
    AllocatorMock<128> alloc_mock;
    FixedBlockQueue<size_t> queue;
    for (size_t i = 0; i < 1000; ++i) {
        queue.push_back(1, &alloc_mock);
        size_t item = queue.front(&alloc_mock);
        queue.pop_front(&alloc_mock);
        EXPECT_EQ(item, 1U);
    }
    EXPECT_TRUE(queue.empty());
}


TEST(FixedBlockQueue, PackEnqueuePackDequeue) {
    AllocatorMock<128> alloc_mock;
    FixedBlockQueue<size_t> queue;
    for (size_t i = 0; i < 20; ++i)
        queue.push_back(i, &alloc_mock);
    for (size_t i = 0; i < 20; ++i) {
        size_t item = queue.front(&alloc_mock);
        queue.pop_front(&alloc_mock);
        EXPECT_EQ(item, i);
    }
    EXPECT_TRUE(queue.empty());
}


TEST(FixedBlockQueue, CrawlingEnqueueDequeue) {
    AllocatorMock<128> alloc_mock;
    FixedBlockQueue<size_t> queue;
    queue.push_back(0, &alloc_mock);
    for (size_t i = 0; i < 1000; ++i) {
        queue.push_back(1 + i, &alloc_mock);
        size_t item = queue.front(&alloc_mock);
        queue.pop_front(&alloc_mock);
        EXPECT_EQ(item, i);
    }
    size_t item = queue.front(&alloc_mock);
    queue.pop_front(&alloc_mock);
    EXPECT_EQ(item, 1000U);
    EXPECT_TRUE(queue.empty());
}


class DoubleCtorException: public std::exception {
public:
    DoubleCtorException(void* ptr) {
        sprintf(&buf[0], "Double ctor call on %p", ptr);
    }

    const char* what() const noexcept override {
        return &buf[0];
    }

protected:
    char buf[128];
};


class InvalidDtorException: public std::exception {
public:
    InvalidDtorException(void* ptr) {
        sprintf(&buf[0], "Invalid dtor call on %p", ptr);
    }

    const char* what() const noexcept override {
        return &buf[0];
    }

protected:
    char buf[128];
};


class CtorDtorTracker {
public:
    void markCtor(void* ptr) noexcept(false) {
        auto rpair = active_objects.insert(ptr);
        if (!rpair.second) {
            throw DoubleCtorException(ptr);
        }
    }

    void markDtor(void* ptr) noexcept(false) {
        size_t count = active_objects.erase(ptr);
        if (count != 1)
            throw InvalidDtorException(ptr);
    }

    bool empty() const {
        return active_objects.empty();
    }

protected:
    std::set<void*> active_objects;
};


class TrackIt {
public:
    TrackIt(CtorDtorTracker* tracker_)
        : tracker(tracker_)
    {
        tracker->markCtor(this);
    }

    TrackIt(const TrackIt& copy)
        : tracker(copy.tracker)
    {
        tracker->markCtor(this);
    }

    ~TrackIt() noexcept(false) {
        try {
            tracker->markDtor(this);
        } catch (...) {
            if (!std::uncaught_exception())
                throw;
        }
    }

protected:
    CtorDtorTracker* tracker;
};


TEST(FixedBlockQueue, CrawlingTrackingEnqueueDequeue) {
    AllocatorMock<128> alloc_mock;
    FixedBlockQueue<TrackIt> queue;
    CtorDtorTracker tracker;
    queue.push_back(TrackIt(&tracker), &alloc_mock);
    for (size_t i = 0; i < 1000; ++i) {
        queue.push_back(TrackIt(&tracker), &alloc_mock);
        queue.pop_front(&alloc_mock);
    }
    queue.pop_front(&alloc_mock);
    EXPECT_TRUE(tracker.empty());
    EXPECT_TRUE(queue.empty());
}


TEST(FixedBlockQueue, TrackingPackEnqueuePackDequeue) {
    AllocatorMock<128> alloc_mock;
    FixedBlockQueue<TrackIt> queue;
    CtorDtorTracker tracker;
    for (size_t i = 0; i < 20; ++i)
        queue.push_back(TrackIt(&tracker), &alloc_mock);
    for (size_t i = 0; i < 20; ++i)
        queue.pop_front(&alloc_mock);
    EXPECT_TRUE(tracker.empty());
    EXPECT_TRUE(queue.empty());
}
