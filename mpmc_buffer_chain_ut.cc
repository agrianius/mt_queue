#include "mpmc_buffer_chain.hh"
#include "ut_helpers.hh"
#include "gtest/gtest.h"
#include <thread>
#include <algorithm>

using mt_queue::BufferChain;

TEST(BufferChain, EnqueueDequeueSomeItems) {
  constexpr ::uint64_t VAL_LIMIT = 1000000;
  BufferChain<::uint64_t> queue{VAL_LIMIT, 1};
  for (::uint64_t val = 42; val < VAL_LIMIT; ++val) {
    queue.Enqueue(val);
  }
  for (::uint64_t val = 42; val < VAL_LIMIT; ++val) {
    auto elem = queue.Dequeue();
    ASSERT_NE(elem, queue.NO_VALUE);
    EXPECT_EQ(elem, val);
  }
}


TEST(BufferChain, Enqueue3Thread_Dequeue1Thread_PointerElem) {
    constexpr int NUMBER_OF_PRODUCERS = 3;
    constexpr int NUMBER_OF_ITEMS = 10000000;
    constexpr int DIFF_GAP = 10 * NUMBER_OF_ITEMS;

    BufferChain<::uint64_t> myqueue{
      NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS, NUMBER_OF_PRODUCERS + 1};
    SyncStart sync(NUMBER_OF_PRODUCERS);

    auto producer_lambda = [&](::uint64_t start_num) {
        sync.WaitForGreenLight();
        for (::uint64_t i = 1; i <= NUMBER_OF_ITEMS; ++i)
            myqueue.Enqueue(i + start_num);
    };

    ::std::thread producers[NUMBER_OF_PRODUCERS];
    ::uint64_t last_items[NUMBER_OF_PRODUCERS];
    for (::uint64_t i = 0; i < NUMBER_OF_PRODUCERS; ++i) {
        producers[i] = ::std::thread(producer_lambda, i * DIFF_GAP);
        last_items[i] = i * DIFF_GAP;
    }

    sync.Start();
    for (::uint64_t item_count = 0;
         item_count < NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;)
    {
      ::uint64_t item = myqueue.Dequeue();
      if (item == myqueue.NO_VALUE)
        continue;

      ::uint64_t producer_num = item / DIFF_GAP;
      EXPECT_EQ(last_items[producer_num] + 1, item);
      last_items[producer_num] = item;
      ++item_count;
    }

    for (auto& producer: producers)
        producer.join();
}


TEST(BufferChain, Enqueue2Threads_Dequeue2Threads) {
    constexpr uint16_t NUMBER_OF_PRODUCERS = 2;
    constexpr uint16_t NUMBER_OF_CONSUMERS = 2;
    constexpr uint32_t NUMBER_OF_ITEMS = 10000000;
    constexpr uint32_t TOTAL_NUMBER_OF_ITEMS =
	NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;
    constexpr uint64_t DIFF_GAP = 1UL << 32UL;

    BufferChain<::uint64_t, 16> myqueue{
      TOTAL_NUMBER_OF_ITEMS, NUMBER_OF_PRODUCERS + NUMBER_OF_CONSUMERS};
    SyncStart sync(NUMBER_OF_PRODUCERS + NUMBER_OF_CONSUMERS);

    ::std::atomic<uint32_t> consume_counter = 0;
    ::std::atomic<uint32_t> source_counter = 1;

    auto producer_lambda = [&](::uint64_t start_num) noexcept {
        sync.WaitForGreenLight();
        for (::uint32_t i = 1; i <= NUMBER_OF_ITEMS; ++i) {
            auto num = source_counter.fetch_add(
                1UL, ::std::memory_order_seq_cst);
            myqueue.Enqueue(start_num + num);
        }
    };

    struct ConsumeRecord {
        uint16_t producer_num;
        uint16_t consumer_num;
        uint32_t serial;
        uint32_t consume_serial;
    };
    ::std::vector<ConsumeRecord> records[NUMBER_OF_CONSUMERS];
    for (auto& rec: records) {
      rec.reserve(TOTAL_NUMBER_OF_ITEMS);
    }

    auto consumer_lambda = [&](uint16_t consumer_num) noexcept {
        records[consumer_num].resize(TOTAL_NUMBER_OF_ITEMS);
        records[consumer_num].resize(0);
        ASSERT_EQ(records[consumer_num].capacity(), TOTAL_NUMBER_OF_ITEMS);
        sync.WaitForGreenLight();
        for (;;) {
            auto item_counter =
                consume_counter.load(::std::memory_order_acquire);
            if (item_counter == TOTAL_NUMBER_OF_ITEMS)
                break;
            auto elem = myqueue.Dequeue();
            if (elem == myqueue.NO_VALUE)
                continue;
            uint32_t consume_serial =
                consume_counter.fetch_add(1UL, ::std::memory_order_seq_cst);
            records[consumer_num].emplace_back(ConsumeRecord{
                    .producer_num = static_cast<uint16_t>(elem >> 32UL),
                    .consumer_num = consumer_num,
                    .serial = static_cast<uint32_t>(elem),
                    .consume_serial = consume_serial});
        }
    };

    ::std::thread producers[NUMBER_OF_PRODUCERS];
    for (uint16_t i = 0; i < NUMBER_OF_PRODUCERS; ++i)
        producers[i] = ::std::thread(producer_lambda, i * DIFF_GAP);

    ::std::thread consumers[NUMBER_OF_CONSUMERS];
    for (uint16_t i = 0; i < NUMBER_OF_CONSUMERS; ++i)
        consumers[i] = ::std::thread(consumer_lambda, i);

    sync.Start();

    for (auto& producer: producers)
        producer.join();
    for (auto& consumer: consumers)
        consumer.join();

    // verify consume records
    ::std::vector<ConsumeRecord> merged;
    merged.reserve(TOTAL_NUMBER_OF_ITEMS);
    for (auto& rec: records)
        merged.insert(merged.end(), rec.begin(), rec.end());
    auto compare_lambda =
        [](const ConsumeRecord& left, const ConsumeRecord& right) noexcept {
            return left.serial < right.serial;
        };
    ::std::sort(merged.begin(), merged.end(), compare_lambda);

    ::uint32_t expect = 1;
    for (auto& elem: merged) {
        ASSERT_EQ(elem.serial, expect);
        ++expect;
    }
}



TEST(BufferChain, Enqueue2Threads_Dequeue2Threads_Perf) {
    constexpr uint16_t NUMBER_OF_PRODUCERS = 2;
    constexpr uint16_t NUMBER_OF_CONSUMERS = 2;
    constexpr uint32_t NUMBER_OF_ITEMS = 10000000;
    constexpr uint32_t TOTAL_NUMBER_OF_ITEMS =
	NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;
    constexpr uint64_t DIFF_GAP = 1UL << 32UL;

    BufferChain<::uint64_t> myqueue{
      TOTAL_NUMBER_OF_ITEMS, NUMBER_OF_PRODUCERS + NUMBER_OF_CONSUMERS};
    SyncStart sync(NUMBER_OF_PRODUCERS + NUMBER_OF_CONSUMERS);

    auto producer_lambda = [&](::uint64_t start_num) noexcept {
        sync.WaitForGreenLight();
        for (::uint32_t i = 1; i <= NUMBER_OF_ITEMS; ++i) {
            myqueue.Enqueue(start_num + i);
        }
    };

    ::std::atomic<::uint32_t> item_counter{0};

    auto consumer_lambda = [&](uint16_t consumer_num) noexcept {
        sync.WaitForGreenLight();
        for (;;) {
            auto elem = myqueue.Dequeue();
            if (elem == myqueue.NO_VALUE) {
              auto counter =
                item_counter.load(::std::memory_order_acquire);
              if (counter == TOTAL_NUMBER_OF_ITEMS)
                break;
              continue;
            }
            auto cnt = item_counter.fetch_add(1U, ::std::memory_order_relaxed);
            if (cnt = TOTAL_NUMBER_OF_ITEMS - 1)
              break;
        }
    };

    ::std::thread producers[NUMBER_OF_PRODUCERS];
    for (uint16_t i = 0; i < NUMBER_OF_PRODUCERS; ++i)
        producers[i] = ::std::thread(producer_lambda, i * DIFF_GAP);

    ::std::thread consumers[NUMBER_OF_CONSUMERS];
    for (uint16_t i = 0; i < NUMBER_OF_CONSUMERS; ++i)
        consumers[i] = ::std::thread(consumer_lambda, i);

    sync.Start();

    for (auto& producer: producers)
        producer.join();
    for (auto& consumer: consumers)
        consumer.join();
}
