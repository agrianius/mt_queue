/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include <atomic>
#include <stddef.h>

/*
 * The goal for the class is to synchronize beginning of execution
 * for many threads.
 */
class SyncStart {
public:
    /*
     * state NumberOfWaitingThreads > 1:
     *    expecting more threads to wait for the green light
     * state NumberOfWaitingThreads == 1:
     *    all expected threads are waiting for the green light
     * state NumberOfWaitingThreads == 0:
     *    the green light is on, go!
     */

    SyncStart(size_t number_of_threads)
        : NumberOfWaitingThreads{number_of_threads + 1}
    {}

    void WaitForGreenLight() {
        // this thread is ready to go
        NumberOfWaitingThreads.fetch_sub(1, std::memory_order_relaxed);
        // wait for the green light
        while (NumberOfWaitingThreads.load(std::memory_order_relaxed))
            ;
    }

    void Start() {
        // waiting for all expected threads ready to go
        while (NumberOfWaitingThreads.load(std::memory_order_relaxed) != 1)
            ; // empty loop
        // Go!
        NumberOfWaitingThreads.store(0, std::memory_order_relaxed);
    }

protected:
    std::atomic<size_t> NumberOfWaitingThreads = {0};
};
