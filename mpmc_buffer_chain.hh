#pragma once
#include <array>
#include <atomic>
#include <cstdint>
#include <memory>
#include <new>
#include <vector>
#include <utility>

namespace mt_queue {


template <class Type>
struct CachlinePadding {
  static constexpr ::uint16_t CACHE_LINE_SIZE = 64;
  char padding[CACHE_LINE_SIZE - sizeof(Type)];
};


template <class ITEM_TYPE>
struct DefaultTypeSpec {
  static constexpr ITEM_TYPE NoValue() noexcept {
    return ITEM_TYPE{0};
  }
};




template <class ITEM_TYPE, ::uint32_t CHUNK_SIZE>
struct Chunk {
  using ThreadCounterType = ::uint32_t;
  using SlotCounterType = ::uint16_t;

  ::std::atomic<::uint64_t> next_chunk;
  CachlinePadding<ThreadCounterType> pad1;
  ::std::atomic<ThreadCounterType> use_counter{0};
  CachlinePadding<ThreadCounterType> pad2;
  ::std::atomic<SlotCounterType> tail;
  ::std::atomic<SlotCounterType> head;
  CachlinePadding<SlotCounterType> pad4;
  ::std::array<::std::atomic<ITEM_TYPE>, CHUNK_SIZE> elems;
};


template <class ITEM_TYPE, ::size_t CHUNK_SIZE = 2048, class TypeSpec = DefaultTypeSpec<ITEM_TYPE>>
class BufferChain {
public:
  using ChunkType = Chunk<ITEM_TYPE, CHUNK_SIZE>;
  using ThreadCounterType = ChunkType::ThreadCounterType;
  using SlotCounterType = ChunkType::SlotCounterType;

  BufferChain(::size_t capacity, ThreadCounterType number_of_threads) noexcept {
    number_of_chunks_ = (capacity - 1) / CHUNK_SIZE + 1 + (number_of_threads - 1) * 2;
    chunks_.reset(new (std::nothrow) ChunkType[number_of_chunks_]);
    for (::uint64_t i = 0; i < number_of_chunks_; ++i) {
      auto& chunk = chunks_.get()[i];
      ::std::fill(
          ::std::begin(chunk.elems),
          ::std::end(chunk.elems),
          TypeSpec::NoValue());
    }
    // lock first chunk
    auto& first_chunk = chunks_.get()[0];
    first_chunk.use_counter.store(LOCK_BIT, ::std::memory_order_relaxed);
    first_chunk.tail.store(0, ::std::memory_order_relaxed);
    first_chunk.head.store(0, ::std::memory_order_relaxed);
    first_chunk.next_chunk.store(NULL_CHUNK, ::std::memory_order_relaxed);
  }

  void Enqueue(ITEM_TYPE val) noexcept {
    ::uint64_t my_chunk_id = enqueue_chunk_.load(::std::memory_order_acquire);
    for (;;) {
      auto& my_chunk = chunks_.get()[my_chunk_id];
      // use-lock my chunk
      my_chunk.use_counter.fetch_add(USE_BIT, ::std::memory_order_acquire);
      {
        ::uint64_t check_it = enqueue_chunk_.load(::std::memory_order_relaxed);
        if (my_chunk_id != check_it) {
          my_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
          my_chunk_id = check_it;
          continue;
        }
      }
      for (;;) {
        SlotCounterType my_slot = my_chunk.tail.load(::std::memory_order_acquire);
        if (my_slot == CHUNK_SIZE) {
          // damn it's full, let's get another chunk
          break;
        }
        ITEM_TYPE expect = NO_VALUE;
        bool success = my_chunk.elems[my_slot].compare_exchange_strong(
            expect, val,
            ::std::memory_order_acq_rel, ::std::memory_order_acquire);
        if (my_chunk.tail.compare_exchange_strong(
                my_slot, my_slot + 1,
                ::std::memory_order_acq_rel, ::std::memory_order_acquire)) {
          ++my_slot;
        }
        if (success) {
          if (my_slot == CHUNK_SIZE) {
            FindNextFreeChunk(my_chunk_id);
          }
          my_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
          return;
        }
      }
      my_chunk_id = FindNextFreeChunk(my_chunk_id);
      my_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
    }
  }

  ITEM_TYPE Dequeue() noexcept {
    ::uint64_t my_chunk_id = dequeue_chunk_.load(::std::memory_order_acquire);
    for (;;) {
      auto& my_chunk = chunks_.get()[my_chunk_id];
      // use-lock my chunk
      ThreadCounterType old_cnt = my_chunk.use_counter.fetch_add(
          USE_BIT, ::std::memory_order_acquire);
      if ((old_cnt & LOCK_BIT) == 0) {
        // oops, we are too slow
        my_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
        my_chunk_id = dequeue_chunk_.load(::std::memory_order_acquire);
        continue;
      }
      {
        ::uint64_t check_it = dequeue_chunk_.load(::std::memory_order_relaxed);
        if (my_chunk_id != check_it) {
          my_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
          my_chunk_id = check_it;
          continue;
        }
      }

      SlotCounterType my_slot = my_chunk.head.load(::std::memory_order_acquire);
      for (;;) {
        if (my_slot == CHUNK_SIZE) {
          // damn, it's done, let's get next chunk
          my_chunk_id = JustToNextChunk(my_chunk_id);
          if (my_chunk_id == NULL_CHUNK) {
            return NO_VALUE;
          }
          break;
        }
        SlotCounterType ready_edge = my_chunk.tail.load(::std::memory_order_acquire);
        if (my_slot == ready_edge) {
          my_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
          return NO_VALUE;
        }
        if (!my_chunk.head.compare_exchange_strong(
                my_slot, my_slot + 1,
                ::std::memory_order_acq_rel, ::std::memory_order_acquire)) {
          continue;
        }
        ITEM_TYPE result = my_chunk.elems[my_slot].load(::std::memory_order_relaxed);
        if (my_slot == CHUNK_SIZE - 1) {
          JustToNextChunk(my_chunk_id);
        } else {
          my_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
        }
        return result;
      }
    }
  }

  static constexpr ITEM_TYPE NO_VALUE = TypeSpec::NoValue();

private:
  static constexpr ThreadCounterType USE_BIT = 2;
  static constexpr ThreadCounterType LOCK_BIT = 1;
  static constexpr ::uint64_t NULL_CHUNK = -1UL;

  ::uint64_t number_of_chunks_;
  std::unique_ptr<ChunkType[]> chunks_;

  CachlinePadding<::uint64_t> pad0;
  std::atomic<::uint64_t> dequeue_chunk_{0};
  CachlinePadding<::uint64_t> pad1;
  std::atomic<::uint64_t> enqueue_chunk_{0};
  CachlinePadding<::uint64_t> pad2;

  ::uint64_t FindNextFreeChunk(::uint64_t const tail_chunk_id) noexcept {
    auto& tail_chunk = chunks_.get()[tail_chunk_id];
    ::uint64_t next_chunk_id =
        tail_chunk.next_chunk.load(::std::memory_order_acquire);
    if (next_chunk_id != NULL_CHUNK) {
      ::uint64_t expect = tail_chunk_id;
      if (enqueue_chunk_.compare_exchange_strong(
          expect, next_chunk_id,
          ::std::memory_order_relaxed, ::std::memory_order_acquire)) {
        return next_chunk_id;
      } else {
        return expect;
      }
    }
    // we have to find new chunk
    next_chunk_id = tail_chunk_id;
    for (;;) {
      ++next_chunk_id;
      next_chunk_id %= number_of_chunks_;
      auto& next_chunk = chunks_.get()[next_chunk_id];
      if (next_chunk.use_counter.load(::std::memory_order_acquire) != 0) {
        continue;
      }
      ThreadCounterType expect = 0;
      bool success = next_chunk.use_counter.compare_exchange_strong(
          expect, LOCK_BIT,
          ::std::memory_order_acq_rel, ::std::memory_order_relaxed);
      if (!success) {
        continue;
      }
      next_chunk.tail.store(0, ::std::memory_order_relaxed);
      next_chunk.head.store(0, ::std::memory_order_relaxed);
      next_chunk.next_chunk.store(NULL_CHUNK, ::std::memory_order_relaxed);
      ::std::fill(
          ::std::begin(next_chunk.elems),
          ::std::end(next_chunk.elems),
          NO_VALUE);
      ::uint64_t expect_null_chunk = NULL_CHUNK;
      success = tail_chunk.next_chunk.compare_exchange_strong(
          expect_null_chunk, next_chunk_id,
          ::std::memory_order_acq_rel, ::std::memory_order_acquire);
      if (success) {
        ::uint64_t expect_tail_chunk_id = tail_chunk_id;
        if (enqueue_chunk_.compare_exchange_strong(
                expect_tail_chunk_id, next_chunk_id,
                ::std::memory_order_acq_rel, ::std::memory_order_acquire)) {
          return next_chunk_id;
        } else {
          return expect_tail_chunk_id;
        }
      } else {
        next_chunk.use_counter.fetch_sub(LOCK_BIT, ::std::memory_order_release);
        ::uint64_t expect_enqueue_chunk_id = tail_chunk_id;
        if (enqueue_chunk_.compare_exchange_strong(
                expect_enqueue_chunk_id, expect_null_chunk,
                ::std::memory_order_relaxed, ::std::memory_order_acquire)) {
          return expect_null_chunk;
        } else {
          return expect_enqueue_chunk_id;
        }
      }
    }
  }

  ::uint64_t JustToNextChunk(::uint64_t const head_chunk_id) noexcept {
    auto& head_chunk = chunks_.get()[head_chunk_id];
    ::uint64_t next_chunk = head_chunk.next_chunk.load(::std::memory_order_acquire);
    if (next_chunk == NULL_CHUNK) {
      head_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
      return NULL_CHUNK;
    }
    ::uint64_t expect = head_chunk_id;
    if (dequeue_chunk_.compare_exchange_strong(
            expect, next_chunk,
            ::std::memory_order_acq_rel, ::std::memory_order_acquire)) {
      head_chunk.use_counter.fetch_sub(USE_BIT | LOCK_BIT, ::std::memory_order_release);
      return next_chunk;
    } else {
      head_chunk.use_counter.fetch_sub(USE_BIT, ::std::memory_order_release);
      return expect;
    }
  }
};


}  // namespace mt_queue
